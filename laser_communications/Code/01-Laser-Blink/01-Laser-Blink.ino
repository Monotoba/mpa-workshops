#include <SoftwareSerial.h>

/*
  Laser Blink

  Turns an Laser Diode on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  This example code is in the public domain.
                                                                                                        
  http://www.arduino.cc/en/Tutorial/Blink
*/
int LASER_OUT = 5;      // Laser control pin
int PHOTOTRANS_IN = 3;  // Photo transitor sense pin
int DELAY_MS = 2000;    // 2000 ms (2 Seconds) cycle period
static int val = 0;     // Memory buffer for input string


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LASER_OUT, OUTPUT);
  pinMode(PHOTOTRANS_IN, INPUT);  

}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LASER_OUT, HIGH);        // turn the LED on (HIGH is the voltage level)
  delay(DELAY_MS/2);                    // wait for a second
  digitalWrite(LASER_OUT, LOW);         // turn the LED off by making the voltage LOW
  delay(DELAY_MS/2);                    // wait for a second 
}
