/**
 * File: STEP 2 - Photo Sensor
 * 
 * AUTH: Randall Morgan
 *
 * LIC: This code is released into the public domain.
 * 
 * DESC: This program tests the photo sensor circuit.
 * You should be able to shine a red laser pointer at
 * your photo transistor and read athe input in the 
 * serial monitor.
 *
 */


int laserPin = 2;   // Laser control pin
int detectPin = 4;  // Photo detector input pin
int pinSate;

void setup() {
  // Setup control and sense pins
  pinMode(laserPin, OUTPUT);
	pinMode(detectPin, INPUT);

  // Initialize Hardware Serial Port
 	Serial.begin(57600);
  
	// Wait for connection to host PC
	while(!Serial) {};
	Serial.print("Connected");
}

void loop() {
  // put your main code here, to run repeatedly:
  pinState = digitalRead(detectPin);
	if(pinState==true){
		Serial.print("1");
	} else {
		Serial.print("0");
	}
	Serial.print("Test");
	delay(500);
}