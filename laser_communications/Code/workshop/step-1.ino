/**
 * File: STEP 1 - LASER DRIVER
 * 
 * AUTH: Randall Morgan
 *
 * LIC: This code is released into the public domain.
 * 
 * DESC: This program tests the laser driver circuit 
 * you built in step 1. If the driver and code are 
 * correct, you'll see the laser flash on and off at
 * a rate of 1Hz.
 *
 */


int laserPin = 2;   // Laser control pin
int detectPin = 3;  // Photo detector input pin

void setup() {
  // put your setup code here, to run once:
  pinMode(laserPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(laserPin, LOW);
  delay(500);
  digitalWrite(laserPin, HIGH);
  delay(500);
}