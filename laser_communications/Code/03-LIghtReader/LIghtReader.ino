/*
   LightReader
   By: Randall Morgan

   This test circuit simply toggles the laser connected
   to digital pin D3 on and off at a rate of 1/2 Hertz. 
   After toggling the laser's state it then reads the 
   input from the photo transistor on digital pin 2.

   To run this program you'll need an Arduino board
   with a laser  diode connected via an npn switching 
   transistor to digital pin 3.

   In addition you'll need a photo transistor connected 
   to a resistor in a pull-down arrangment and the 
   junction of the pull-up resistor and the photo 
   transistor collector connected to digital pin 2 on
   the Arduino board.

   See the article at: http://www.randallmorgan.me/blog/
   Search for Arduino Laser Communications 

   This code is placed in the public domain
*/

// Tell the IDE we want to use 
// the SoftwareSerial library
#include <SoftwareSerial.h>

int LASER_OUT = 2;      // Laser control pin
int PHOTOTRANS_IN = 3;  // Photo transitor sense pin
int DELAY_MS = 1000;    // 2000 ms (2 Seconds) cycle period
int val = 0;            // input value
char buf[100];


SoftwareSerial laserSerial(PHOTOTRANS_IN, LASER_OUT);

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LASER_OUT, OUTPUT);
  pinMode(PHOTOTRANS_IN, INPUT);  

  // Set up serial port
  Serial.begin(4800);
  // Wait for hardware serial to connect
  while(!Serial) {
    // wiating for connection....
  }

  Serial.print("Connected\n");

  // Setup Software Serial
  laserSerial.begin(4800);

  // Now we start sending data to 
  // allow the users to line up the
  // lasers and photo transistors.
  // Once the user hits a key, we 
  // stop and begin transceiving 
  // data on the serial ports.
  while(!Serial.available()){ 
    laserSerial.write("Light is beautiful!...");  
  }
  
}


// the loop function runs over and over again forever
void loop() {
  // If the photo transistor has received 
  // and data, send it to the host
  if(laserSerial.available()) {
    Serial.write(laserSerial.read());
  }

  if(Serial.available()) {
    laserSerial.write(Serial.read());
  }
}
