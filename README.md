# MPA-Workshops

This repository will be used to organize and make available all required files 
for my talks and workshops given to the Montana Programmers Association meembers.

All code and documentation placed here is released into the public domain unless 
otherwise specified. You are free to use the files from this repository. However, 
a curtious attribution would be appreciated. 

_Note: I am currently in the process of moving files into this repo. If what 
you're looking for isn't here, please contact me or leave an issue and I'll 
rectify the situation._

## Projects
* Laser Communications

## Talks


