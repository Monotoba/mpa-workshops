# Arduino Laser Communications Project
## Using Lasers to Send Serial Data

This project is a simple project meant to be completed in a single evening (3-4 hours).
The project uses any arduino uno compatible sbc and the software serial library to form 
a duplex communications channel over a laser beam, using both hardware and software driven 
serial ports. 

A small laser diode module is used for the transmitting device and a matched photo 
transitor is used for the detecting device. 

This is a great project to teach serial communications. Slides are included in the slides 
directory. The source images used to create the slides are included in the images directory.

The Fritzing or Arduino Studio cab be used as well as command line tools. 


