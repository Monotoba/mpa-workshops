/**
 * File: STEP 3 - Recieving Data
 * 
 * AUTH: Randall Morgan
 *
 * LIC: This code is released into the public domain.
 * 
 * DESC: This program tests the photo sensor circuit.
 * You should be able to shine a red laser pointer at
 * your photo transistor and read athe input in the 
 * serial monitor.
 *
 */
#include <SoftwareSerial.h>
#include <stdio.h>  // for sptinf()

int laserPin = 2;   // Laser control pin
int detectPin = 4;  // Photo detector input pin
char buf[100];

SoftwareSerial laserSerial(3, 2);	 // RX/TX

void setup() {
  
  // Initialize Hardware Serial Port
 	Serial.begin(57600);
  
	// Wait for connection to host PC
	while(!Serial) {};
	Serial.print("Connected\n");
  
  laserSerial.begin(1200);
}

void loop() {
	// Recieve data on Software Serial 
	// pass it on to hardware serial for
	// host PC to display.
  if(laserSerial.available()) {
		Serial.write(laserSerial.read());
	}

  if(Serial.available()) {
    laserSerial.write(Serial.read());
  }

  
}