/**
 * File: Send Test Data
 * 
 * AUTH: Randall Morgan
 *
 * LIC: This code is released into the public domain.
 * 
 * DESC: This program tests the photo sensor circuit.
 * You should be able to shine a red laser pointer at
 * your photo transistor and read athe input in the 
 * serial monitor.
 *
 */
#include <SoftwareSerial.h>

int laserPin = 2;   // Laser control pin
int detectPin = 4;  // Photo detector input pin

SoftwareSerial laserSerial(3, 2);  // RX, TX

void setup() {
  // Initialize Hardware Serial Port
  Serial.begin(57600);
  
  // Wait for connection to host PC
  while(!Serial) {};
  Serial.print("Connected\n");

  // Setup software serial
  laserSerial.begin(1200);
  laserSerial.print("Laser Armed!\n");
  
}

void loop() {

  laserSerial.write("Laser Armed!\n");
  delay(100);
   
}